This directory contains a SOWH test, implemented by [SOWHAT](https://github.com/josephryan/sowhat), to evaluate 
how well the gene presence/ absence data support a monophyletic clade comprised of 
Ctenophora and Porifera. The test was executed as follows:

    sowhat --dir=ctpo_sowhat.out --constraint=ctpo.tre --aln=min2.I1.5.phy --name=ctpo_sowh --model=MULTIGAMMA --reps=1000 --rax='raxmlHPC-PTHREADS-SSE3 -T 5 -K GTR -g known_relationships.tre' > sowhat.out 2> sowhat.err &
