#!/bin/bash
#SBATCH -J choanimali_genome1
#SBATCH --nodes=2
#SBATCH --ntasks-per-node=20
#SBATCH --constraint="e5-2670"
#SBATCH --partition=ivy-batch

#SBATCH -t 168:00:00
#SBATCH -e 1-%j.out
#SBATCH -o 2-%j.out

module load phylobayes/1.3b-mpi

srun pb_mpi -d genome.choanimalia.phy choanimali_genome_chain1
