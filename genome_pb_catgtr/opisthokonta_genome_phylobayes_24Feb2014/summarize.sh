module load phylobayes/1.3b-mpi

bpcomp -x 1500 10 opisthokonta_genome_chain1 opisthokonta_genome_chain2 
tracecomp -x 1500 opisthokonta_genome_chain1 opisthokonta_genome_chain2

bpcomp -o chain1 -x 1500 10 opisthokonta_genome_chain1
bpcomp -o chain2 -x 1500 10 opisthokonta_genome_chain2

