#!/bin/bash
#SBATCH -J holozoa2
#SBATCH --nodes=2
#SBATCH --ntasks-per-node=20
#SBATCH --constraint="e5-2670"
#SBATCH --partition=ivy-batch

#SBATCH -t 168:00:00
#SBATCH -e holozoa-1-%j.out
#SBATCH -o holozoa-2-%j.out

module load phylobayes/1.3b-mpi

srun pb_mpi -d est.holozoa.phy holozoa_chain2
