#!/bin/bash

#SBATCH -J choanimalia2
#SBATCH --nodes=3-3
#SBATCH --ntasks-per-node=16
#SBATCH -n 48
#SBATCH --mem=60G
#SBATCH -C e5-2600
#SBATCH --qos=epscor-condo

#SBATCH -t 168:00:00
#SBATCH -e 1-%j.out
#SBATCH -o 2-%j.out

module load phylobayes/1.3b-mpi

srun pb_mpi -d est.choanimalia.phy choanimalia_chain2
